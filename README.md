`baana init todo`

- init app

`baana generate model todo`

- generate model

`vim models/todo.go`

-  write model code

`vim controllers`

- add controller code

`dep init`

- dependency

`baana generate migration create_todo_table`

- Add migration

`go build`

- build code

`vim config/db.json`

- Add DB credentials

`BAANA_ENV=development ./todo migrate up ""`

- Migrate

`BAANA_ENV=development ./todo server`

- Run
