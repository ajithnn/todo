package controllers

import (
	"github.com/amagimedia/todo/models"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/ajithnn/baana/service"
)

type TodoController struct {
	*models.Todo
	db *gorm.DB
	*service.Query
}

func Todo(db *gorm.DB) interface{} {
	handle := &TodoController{&models.Todo{}, db, &service.Query{}}
	return handle
}

// swagger:operation POST /todos createTodo
//
// Creates a new Todo with the given parameters.
//
//
// ---
// produces:
// - application/json
// parameters:
// responses:
func (handler *TodoController) Create(c *gin.Context) {
	if err := c.BindJSON(handler.Todo); err != nil {
		c.Error(err)
		return
	}
	handler.db.Create(handler.Todo)
	c.JSON(200, gin.H{"message": "success"})
	return
}

// swagger:operation PUT /todos/{id} updateTodo
//
// Update a Todo with the given parameters.
//
//
// ---
// produces:
// - application/json
// parameters:
// responses:
func (handler *TodoController) Update(c *gin.Context) {
	if err := c.BindJSON(handler.Todo); err != nil {
		c.Error(err)
		return
	}
	handler.db.Update(handler.Todo)
	c.JSON(200, gin.H{"message": "success"})
	return
}

// swagger:operation GET /todos/ getTodos
//
// Get Todos with the given query filters.
//
//
// ---
// produces:
// - application/json
// parameters:
// responses:
func (handler *TodoController) Read(c *gin.Context) {
	todos := make([]models.Todo, 0)
	handler.db.Model(&models.Todo{}).Find(&todos, handler.Todo)
	c.JSON(200, gin.H{"todos": todos})
	return
}

// swagger:operation DELETE /todos/{id} deleteTodo
//
// Delete Todos  with the given query filter.
//
//
// ---
// produces:
// - application/json
// parameters:
// responses:
func (handler *TodoController) Delete(c *gin.Context) {
	if err := c.BindJSON(handler.Todo); err != nil {
		c.Error(err)
		return
	}
	handler.db.Delete(handler.Todo)
	c.JSON(200, gin.H{"message": "success"})
	return
}
