package migrations

import (
	"fmt"
	"github.com/amagimedia/todo/models"
	"github.com/jinzhu/gorm"
	"gitlab.com/ajithnn/baana/migrator"
)

type migrate struct{}

func Migrate(db *gorm.DB, version, mode string) {

	m := migrate{}

	db.AutoMigrate(&models.Migration{})
	err := migrator.Migrate(db, version, mode, &m)
	if err != nil {
		fmt.Println("Error in migration: " + err.Error())
	}
}

func (m migrate) Create_todo_table_20180906104327(mode string, dbConn *gorm.DB) {
	defer migrator.UpdateMigrations(dbConn, mode)
	switch mode {
	case "up":
		// Write UP migration here.
		dbConn.AutoMigrate(&models.Todo{})
	case "down":
		// Write DOWN migration here.
		dbConn.Exec("drop table todos;")
	}
}
