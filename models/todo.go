package models

import ()

// Todo denotes
//
// Todo is used for
//
// swagger:model Todo
type Todo struct {
	ID     int    `gorm:"primary_key" json:"id"`
	Task   string `json:"task"`
	Status string `json:"status"`
}
