package route

import (
  "gitlab.com/ajithnn/baana/service"

  "github.com/amagimedia/todo/controllers"

)

func Init() {
service.ControllerFuncs = make(map[string]service.HandlerInit)

service.ControllerFuncs["Todo"] = controllers.Todo

}

