package main

import (
	"fmt"
	"github.com/amagimedia/todo/server"
	"github.com/amagimedia/todo/migrations"
	"github.com/amagimedia/todo/route"
	bsvc "gitlab.com/ajithnn/baana/service"
	"os"
	"io/ioutil"
)

func main() {

	if len(os.Args) < 2 {
	fmt.Println("Usage: todo <Mode:('migrate'|'server')>; todo migrate <mode> <version>; todo server")
		os.Exit(1)
	}

	route.Init()


  	dbConf, err := ioutil.ReadFile("config/db.json")
  	if err != nil {
  		panic("Invalid db config / no db config found.")
    		os.Exit(1)
	}

  	svc, err := bsvc.New(dbConf)
  	if err != nil {
    		panic(err)
    		os.Exit(1)
  	}


	switch os.Args[1] {
	case "migrate":
		fmt.Println("Migrating...")
		migrations.Migrate(svc.DB, os.Args[3], os.Args[2])
	case "server":
		server.Run(svc)
	default:
		fmt.Println("Unknown Mode , exiting")
		os.Exit(1)
	}
}
